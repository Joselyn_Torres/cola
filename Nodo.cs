﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoselynTorres3BCola
{
    public class Nodo
    {
        private string Dato;
        private Nodo Siguiente;

        public string dato
        {
            get { return Dato; }
            set { Dato = value; }
        }
        public Nodo siguiente
        {
            get { return Siguiente; }
            set { Siguiente = value; }
        }
    }
}
