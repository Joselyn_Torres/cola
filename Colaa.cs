﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoselynTorres3BCola
{
    public class Colaa
    {
        private Nodo primer = new Nodo();
        private Nodo ultimo = new Nodo();
        public Colaa()
        {
            primer = null;
            ultimo = null;
        }
    
        public void Encolar()
        {
            Nodo nuevonodo = new Nodo();
            Console.WriteLine("\nIngrese el dato:\n");
            nuevonodo.dato = (Console.ReadLine());

            if (primer == null)
            {
                primer = nuevonodo;
                primer.siguiente = null;
                ultimo = nuevonodo;
            }
            else
            {
                ultimo.siguiente = nuevonodo;
                nuevonodo.siguiente = null;
                ultimo = nuevonodo;
            }
        }
        
        public void Desencolar()
        {
            Nodo actual = new Nodo();
            actual = primer;
            if (primer!= null)
            {
                while (actual != null)
                {
                    Console.Write("\n "+ actual.dato);
                    actual = actual.siguiente;
                }
            }
            else
            {
                Console.WriteLine("La cola se encuentra vacía\n");
            }
        }
    }
}
